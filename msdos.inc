	.printl Including MSDOS.INC
	.xlist
;
; MS-DOS Library inclusion file
; Include File for MS-DOS diskette format definitions
; Copyright (c) by Klaus K{mpf Softwareentwicklung 1986
; Version 1.0  21.06.86
; Version 1.1   2.07.86  #boot_record, #fat_record included
; Version 1.2  26.09.86  J-Format (Rainbow) included
;
; Record	= Logical 128-Byte record
; Sector	= physical (512-byte) sector on target disk
; Cluster	= Minimal allocation size in MS-DOS (512 Bytes or 1kbyte)
; LSN		= Logical Sector Number (0..x)
;
; Offsets:
.text		equ	0		;w: text
.rpt		equ	2		;b: Records Per Track
.sides		equ	3		;b: # of sides
.rps		equ	4		;b: Records Per Sector
.secsize	equ	5		;w: Sector size in bytes
.spc		equ	7		;b: Sectors per cluster
.rpc		equ	8		;b: Records per cluster
.spd		equ	9		;w: Sectors per disk
.off		equ	11		;w: Record offset for LSN 0
.fat		equ	13		;w: LSN of 1st FAT sector
.fatlen		equ	15		;b: # of FAT lsns
.fatrpt		equ	16		;b: # of FAT repetitions
.dir		equ	17		;w: LSN of 1st DIR sector
.dirlen		equ	19		;b: # of DIR lsns
.entries	equ	20		;b: # of DIR entries
.data		equ	21		;w: LSN of 1st DATA sector
.datalen	equ	23		;w: # of DATA lsns
.clusters	equ	25		;w: highest cluster #
;
#boot_record	equ	0
.boot_id	equ	21		;ID-Byte at boot + 21
#fat_record	equ	4
#jfat_record	equ	20
.fat_id		equ	0		;ID-Byte at fat + 0
_MINID		equ	0f8h		;Minimal ID Value supported
;
@id_table:
	defb	_MINID
	defw	@ms_ds11		;FF
	defw	@ms_ss11		;FE
	defw	@ms_ds20		;FD
	defw	@ms_ss20		;FC
	defw	@j_ds			;FB
	defw	@j_ss			;FA
	defw	@tos_ds			;F9
	defw	@tos_ss			;F8
;
@ms_ds11:
	defw	@txt_ms_ds11
	defb	  32,   2,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   2,   8		;spc, rpc
	defw	 640,   0		;spd, off
	defw	   1			;fat
	defb	   2,   1		;fatlen, fatrpt
	defw	   3			;dir
	defb	   7, 112		;dirlen, entries
	defw	  10, 630		;data, datalen
	defw	 315+2			;clusters
@txt_ms_ds11:
	defm	'MS-DOS 1.1 Double Sided$'
;
@ms_ss11:
	defw	@txt_ms_ss11
	defb	  32,   1,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   1,   4		;spc, rpc
	defw	 320,   0		;spd, off
	defw	   1			;fat
	defb	   2,   1		;fatlen, fatrpt
	defw	   3			;dir
	defb	   4,  64		;dirlen, entries
	defw	   7, 313		;data, datalen
	defw	 313+2			;clusters
@txt_ms_ss11:
	defm	'MS-DOS 1.1 Single Sided$'
;
@ms_ds20:
	defw	@txt_ms_ds20
	defb	  36,   2,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   2,   8		;spc, rpc
	defw	 720,   0		;spd, off
	defw	   1			;fat
	defb	   2,   2		;fatlen, fatrpt
	defw	   5			;dir
	defb	   7, 112		;dirlen, entries
	defw	  12, 708		;data, datalen
	defw	 354+2			;clusters
@txt_ms_ds20:
	defm	'MS-DOS 2.0 Double Sided$'
;
@ms_ss20:
	defw	@txt_ms_ss20
	defb	  36,   1,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   1,   4		;spc, rpc
	defw	 360,   0		;spd, off
	defw	   1			;fat
	defb	   2,   2		;fatlen, fatrpt
	defw	   5			;dir
	defb	   4,  64		;dirlen, entries
	defw	   9, 351		;data, datalen
	defw	 351+2			;clusters
@txt_ms_ss20:
	defm	'MS-DOS 2.0 Single Sided$'
;
@j_ds:
	defw	@txt_j_ds
	defb	  40,   2,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   2,   8		;spc, rpc
	defw	1600,   0		;spd, off
	defw	  20			;fat
	defb	   3,   2		;fatlen, fatrpt
	defw	  26			;dir
	defb	   6,  96		;dirlen, entries
	defw	  32,1568		;data, datalen
	defw	 784+2			;clusters
@txt_j_ds:
	defm	'J-Format Double Sided$'
;
@j_ss:
	defw	@txt_j_ss
	defb	  40,   1,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   1,   4		;spc, rpc
	defw	 800,   0		;spd, off
	defw	  20			;fat
	defb	   3,   2		;fatlen, fatrpt
	defw	  26			;dir
	defb	   6,  96		;dirlen, entries
	defw	  32, 768		;data, datalen
	defw	 768+2			;clusters
@txt_j_ss:
	defm	'J-Format Single Sided$'
;
@tos_ds:
	defw	@txt_tos_ds
	defb	  36,   2,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   2,   8		;spc, rpc
	defw	1440,   0		;spd, off
	defw	   1			;fat
	defb	   5,   2		;fatlen, fatrpt
	defw	  11			;dir
	defb	   7, 112		;dirlen, entries
	defw	  18,1422		;data, datalen
	defw	 711+2			;clusters
@txt_tos_ds:
	defm	'ATARI ST Double Sided$'
;
@tos_ss:
	defw	@txt_tos_ss
	defb	  36,   1,   4		;rpt, sides, rps
	defw	 512			;secsize
	defb	   2,   8		;spc, rpc
	defw	 720,   0		;spd, off
	defw	   1			;fat
	defb	   5,   2		;fatlen, fatrpt
	defw	  11			;dir
	defb	   7, 112		;dirlen, entries
	defw	  18, 702		;data, datalen
	defw	 351+2			;clusters
@txt_tos_ss:
	defm	'ATARI ST Single Sided$'
	.list
