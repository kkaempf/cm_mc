	.printl Including MCDATA.INC
;
; Error Handling
;
err_no_mem:
	ld	de,txt_no_mem
	jp	error
err_bad_parm:
	ld	de,txt_bad_parm			;"Schlechte Parameter."
	jp	error
err_read_err:
	ld	de,txt_read_err
	jp	error
err_no_src_drv:
	ld	de,txt_no_src_drv
	jp	error
err_bad_src_drv:
	ld	de,txt_bad_src_drv
	jp	error
err_no_src_name:
	ld	de,txt_no_src_name
	jp	error
err_src_not_ready:
	ld	de,txt_src_not_ready
	jp	error
err_read_error:
	ld	de,txt_read_error
	jp	error
err_id_undef:
	ld	de,txt_id_undef
	jp	error
err_write_err:
	ld	de,txt_write_err
	jp	error
err_bad_dest_drv:
	ld	de,txt_bad_dest_drv
	jp	error
err_no_src_entry:
	ld	de,txt_no_src_entry
	jp	error
err_file_exists:
	ld	de,txt_file_exists
	jp	error
err_dest_dir_full:
	ld	de,txt_dest_dir_full
	jp	error
err_cant_close:
	ld	de,txt_cant_close
	jp	error
err_bad_entry:
	ld	de,txt_bad_entry
	jp	error
err_early_end:
	ld	de,txt_early_end
	jp	error
err_src_too_big:
	ld	de,txt_src_too_big
	jp	error
error:
	push	de
	print	txt_err_on
	pop	de
	bdos_call	prtstr
	print	txt_err_off
	jp	quit
;----------------------------------------------------------
;
; bad messages
;
txt_err_on:
	defb	bell,cr,lf
	defm	'*** Fehler:'
	defb	cr,lf,'$'
txt_err_off:
	defb	cr,lf,cr,lf,'$'
txt_no_mem:
	defm	'Zuwenig Speicher !$'
txt_no_src_name:
	defm	'Welches File soll kopiert werden ?'
	defb	cr,lf
	defb	cr,lf
txt_bad_parm:
	defm	'Schlechte Parameter.'
	defb	cr,lf
	defm	'Aufruf mit: MC lw:filename<.ext> <lw:><filename><.ext>'
	defb	cr,lf,'$'
txt_read_err:
	defm	'Lesefehler$'
txt_no_src_drv:
	defm	'Nummer des MSDOS-Laufwerks nicht angegeben'
	defb	cr,lf
	defm	'oder der MSDOS-Filename ist fehlerhaft.$'
txt_bad_src_drv:
	defm	'Fehlerhafte MSDOS-Laufwerksbezeichnung.'
	defb	cr,lf
	defm	'Bitte Laufwerk mit ''A:'' bis ''P:'' angeben.$'
txt_bad_dest_drv:
	defm	'Fehlerhafte CP/M-Laufwerksbezeichnung.'
	defb	cr,lf
	defm	'Bitte Laufwerk mit ''A:'' bis ''P:'' angeben.$'
txt_src_not_ready:
	defm	'Keine Diskette im angegebenen Laufwerk.$'
txt_read_error:
	defm	'Sektor defekt.$'
txt_id_undef:
	defm	'MS-DOS Format unbekannt.$'
txt_no_src_entry:
	defm	'Kann angegebene MSDOS-Datei nicht finden.$'
txt_dest_dir_full:
	defm	'Inhaltsverzeichnis der Ziel-Diskette voll.$'
txt_write_err:
	defm	'Fehler beim Schreiben der Ziel-Datei.$'
txt_cant_close:
	defm	'Fehler beim Schlie~en der Ziel-Datei.$'
txt_bad_entry:
	defm	'MSDOS-Directory fehlerhaft !$'
txt_early_end:
	defm	'Kann MSDOS-Datei beim Lesen nicht erweitern.$'
txt_src_too_big:
	defm	'MSDOS-Datei ist zu gro~ zum kopieren.$'
;
; good messages
;
heading:	defb	cr,lf
		defm	'Datei}bertragung MSDOS => CP/M 2.2'
		defb	cr,lf
		defm	'Copyright (c) by Klaus K{mpf Softwareentwicklung 1986'
		defb	cr,lf,cr,lf,'$'
footing:	defb	'$'
eol:		defb	cr,lf,'$'
;
txt_file_exists:defb	bell
		defm	'*** ACHTUNG:'
		defb	cr,lf
		defm	'Eine Datei unter diesem Namen existiert bereits.'
		defb	cr,lf
		defm	'Trotzdem weitermachen (J/N) ? $'
txt_file_found:	defm	'Quell-Datei gefunden: '
txt_name_found:	defm	'FILENAME.EXT'
		defb	cr,lf,'$'
txt_start_copying:
		defm	'Kopiere   '
from_drive:	defm	'X:'
from_filename:	defm	'NNNNNNNN.EEE'
		defm	'  nach  '
to_drive:	defm	'X:'
to_filename:	defm	'NNNNNNNN.EXT'
		defb	cr,lf,'$'
closing_dest:	defb	cr,lf
		defm	'Kopiervorgang beendet.'
		defb	cr,lf,cr,lf,'$'
reading_source:	defb	cr,'    Lese Sektor '
numrea:		defb	'    H',cr,'$'
writing_dest:	defb	cr,'Schreibe Sektor '
numwrt:		defb	'    H',cr,'$'
;
@src_drive:	defb	0
@src_code:	defb	0
@from_name:	defm	'FILENAMEEXT'
@src_name:	defm	'FILENAMEEXT'
		defs	5+16,0
;
@dest_drive:	defb	0
@dest_code:	defb	0
@to_name:	defm	'FILENAMEEXT'
@dest_fcb:	defb	0			;FCB f}r Zieldatei
@dest_name:	defm	'FILENAMEEXT'
		defs	24,0
;
?@found_entry:	defw	0
?@next_entry:	defw	0
@entry_#:	defb	0
@joker?:	defb	0			;Joker in Filename ?
@found?:	defb	0			;At least 1 file found ?
;
@eof_pos:	defb	0
@data_full?:	defb	0
@data_len:	defb	0
@src_len:	defw	0
@read_len:	defw	0
@write_len:	defw	0
;
;
;----------------------------------------------------------
;
@next_cluster:	defw	0
@cluster:	defw	0
@sec_in_clus:	defb	0
@lsn:		defw	0
@rec_in_sec:	defb	0
@relrec:	defw	0
?@cluster_area:	defw	@data_area
?@lsn_area:	defw	@data_area
?@record_area:	defw	@data_area
?@data_area:	defw	@data_area
@track:		defw	0
@record:	defw	0
	defm	'<*****FAT:*****>'
@fat_area:
	defs	7*512,0
	defm	'<*****DIR:*****>'
@dir_area:
	defs	112*32,0
	defm	'<*****DATA*****>'
@data_area:
