	.printl Including IMMISC.MAC
;
; Get Drive code
; I: @fcb set
; O: @src_code, @src_drive set
;
get_parms:
	ld	a,(@fcb)
	or	a				;Drive given ?
	jp	z,err_no_src_drv
	cp	17
	jp	nc,err_bad_src_drv
	dec	a
	ld	(@src_code),a			;A: = 0
	add	a,'A'
	ld	(@src_drive),a
	ret
;
;
; Get ID Table address by evaluating ID-Byte at Boot+.boot_id
; I: -
; O: IY -> ID Table
;
get_id:
	call	reset_system
	call	select_src
	ld	a,.boot_id
	ld	hl,#boot_record			;Try bootrecord 1st
	call	GetIDRecord
	jr	nc,evaluate_id
	ld	a,.fat_id
	ld	hl,#fat_record			;Then IBM-Fat
	call	GetIDRecord
	jr	nc,evaluate_id
	ld	a,.fat_id
	ld	hl,#jfat_record			;Then J-Format Fat
	call	GetIDRecord
evaluate_id:
	ld	hl,@id_table
	cp	(hl)
	jp	c,err_id_undef
	dec	hl
get_id_loop:
	inc	hl
	inc	hl
	inc	a
	jr	nz,get_id_loop			;HL -> id table pointer
	ld	a,(hl)
	inc	hl
	ld	h,(hl)
	ld	l,a				;HL -> ID table
	push	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	bdos_call	prtstr			;Show what we've found
	print	eol
	print	eol
	pop	iy
	ret
;
;
; get id_record (boot or fat or whatsoever)
; I: A  = id_offset in record
;    HL = record #
; O: nc, if id found > _MINID
;
GetIDRecord:
	ld	(@id_offset),a
	ld	(@relrec),hl
	ld	hl,@dir_area
	push	hl
	ld	(?@dma),hl
	ld	iy,(@id_table+1)		;IY -> Dummy ID
	call	read_record			;Read @relrec to ?@dma
	pop	iy
	ld	a,(iy+<@id_offset: 0>)
	cp	_MINID
	ret
;
;
; Convert @lsn to @relrec
; I: IY -> ID Block
;    @lsn set
; O: @relrec set
;
compute_relrec:
	ld	hl,(@lsn)
	ld	a,(iy+.rps)
	call	multiply			;hl = @lsn * .rps
	
	ld	e,(iy+.off)
	ld	d,(iy+.off+1)
	add	hl,de
	ld	(@relrec),hl
	ret
;
;
; Convert @relrec to @track, @record
; I: IY -> ID Block
;    @relrec set
; O: @track set
;    @record set
;
compute_trkrec:
	ld	hl,(@relrec)
	ld	bc,-1
	ld	e,(iy+.rpt)
	ld	d,0				;de = recs per track
compute_track_loop:
	inc	bc
	or	a
	sbc	hl,de
	jr	nc,compute_track_loop
	add	hl,de
	ld	a,(iy+.sides)
	cp	2				;double sided ?
	jr	z,set_track			;Y: jp
	sla	c				;N: track * 2
	rl	b
set_track:
	ld	(@track),bc
	ld	(@record),hl
	ret
;
;
; Select Destination Drive
; I: @src_code set
; O: drive selected via BIOS-Call
;
select_src:
	ld	a,(@src_code)
	ld	c,a
	ld	e,0
	bios_call	b_seldsk
	ld	a,h
	or	l
	ret	nz
	jp	err_src_not_ready
;
;
; Multiply HL by A
; I: HL, A
; O: HL = HL * A
;
multiply:
	ld	e,a
	ld	d,0
	ld	c,l
	ld	b,h
	ld	hl,0
	ld	a,16
multiply_loop:
	add	hl,hl
	ex	de,hl
	add	hl,hl
	ex	de,hl
	jr	nc,multiply_noadd
	add	hl,bc
multiply_noadd:
	dec	a
	jr	nz,multiply_loop
	ret
;
;
; Print 32 bit # to de
; I: BCHL = 32 Bit #
;    A  = # of digits (6..10)
;    DE -> text string
; O: text string = decimal representation of number
;
number32:
	sub	10
	neg
	ld	(@digits),a			;# of digits to suppress
	xor	a
	ld	(@zero?),a			;none zeroes yet
	push	ix,iy
	ld	ix,@digits32			;ix -> 32 bit digit values
	call	number_digits
	pop	iy,ix
	ld	a,5
	jp	number16x
;
;
; Print 16 bit # to de
; I: HL = 16 Bit #
;    A  = # of digits (5..4)
;    DE -> text string
; O: text string = decimal representation of number
;
number16:
	push	af
	xor	a
	ld	(@zero?),a			;none zeroes yet
	pop	af
	ld	bc,0
number16x:
	sub	5
	neg
	ld	(@digits),a			;# of digits to suppress
	push	ix,iy
	ld	ix,@digits16			;ix -> 16 bit digit values
	call	number_digits
	pop	iy,ix
	ld	a,3
	jp	number8x
;
;
; Print 8 bit # to de
; I: L = 8 Bit #
;    A = # of digits (3..1)
;    DE -> text string
; O: text string = decimal representation of number
;
number8y:
	push	af
	ld	a,0ffh
	ld	(@zero?),a
	pop	af
	ld	bc,0
	ld	h,0
	jr	number8x
number8:
	push	af
	xor	a
	ld	(@zero?),a			;none zeroes yet
	pop	af
	ld	bc,0
	ld	h,0
number8x:
	sub	3
	neg
	ld	(@digits),a			;# of digits to suppress
	push	ix,iy
	ld	ix,@digits8			;ix -> 8 bit digit values
	call	number_digits
	pop	iy,ix
	ld	a,(@zero?)			;Ziffer ausgegeben ?
	or	a
	ret	nz				;Y: ok
	dec	de
	ld	a,'0'				;N: Null
	ld	(de),a
	ret
;
;
; UPRO:
; compute digits for numberx
; I: BCHL = #
;    DE -> string
;    IX -> 32 Bit digit value table
; O: DE -> string+digits
;    BCHL = remainder
;
;
number_digits:
	push	de
	pop	iy				;iy -> string
	ld	e,c
	ld	d,b				;dehl = number
	ld	b,(ix)				;b= values in table
	inc	ix
digit_loop:
	ld	c,'0'-1
number_loop:
	push	de,hl				;save #
	inc	c
	ld	a,l
	sub	(ix+0)
	ld	l,a
	ld	a,h
	sbc	a,(ix+1)
	ld	h,a
	ld	a,e
	sbc	a,(ix+2)
	ld	e,a
	ld	a,d
	sbc	a,(ix+3)
	ld	d,a
	jr	c,set_digit
	pop	af,af				;dummy pop
	jr	number_loop
set_digit:
	pop	hl,de
	ld	a,c
	cp	'0'				;digit = 0 ?
	jr	nz,set_digitnz			;N: jp
	ld	a,(@zero?)			;Digit yet ?
	or	a
	jr	nz,set_digitnz			;Y: set zero
	ld	c,space
	jr	set_digitall
set_digitnz:
	ld	a,0ffh
	ld	(@zero?),a			;mark digit
set_digitall:
	ld	a,<@digits: 0>
	or	a				;set this digit ?
	jr	z,yes_digit			;Y: ok
	dec	a
	ld	(@digits),a
	jr	next_digit			;N: next
yes_digit:
	ld	(iy),c
	inc	iy
next_digit:
      4:inc	ix
	djnz	digit_loop
	ld	c,e
	ld	b,d				;bchl = number
	push	iy
	pop	de				;de -> text string
	ret
;
;
; Data for numberx
;
@digits32:
	defb	5
	defw	51712,15258			;1.000.000.000
	defw	57600,1525			;  100.000.000
	defw	38528,152			;   10.000.000
	defw    16960,15			;    1.000.000
	defw    34464,1				;      100.000
@digits16:
	defb	2
	defw	10000,0				;       10.000
	defw	1000,0				;        1.000
@digits8:
	defb	3
	defw	100,0				;          100
	defw	10,0				;           10
	defw	1,0				;	     1
;
break?:
	push	hl,bc
	bdos_call	drctio,255
	pop	bc,hl
	cp	3
	ret	nz
	jp	quit
;
?bios_call:
	ld	hl,<?@bios: dw 0>
	ld	l,a
	jp	(hl)
;
reset_system:
	bdos_call	13
	ret
