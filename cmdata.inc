	.printl Including CMDATA.INC
;
; Error Handling
;
err_no_mem:
	ld	de,txt_no_mem
	jp	error
err_bad_parm:
	ld	de,txt_bad_parm			;"Schlechte Parameter."
	jp	error
err_read_err:
	ld	de,txt_read_err
	jp	error
err_write_err:
	ld	de,txt_write_err
	jp	error
err_no_dest_drv:
	ld	de,txt_no_dest_drv
	jp	error
err_bad_dest_drv:
	ld	de,txt_bad_dest_drv
	jp	error
err_no_src_name:
	ld	de,txt_no_src_name
	jp	error
err_dest_not_ready:
	ld	de,txt_dest_not_ready
	jp	error
err_id_undef:
	ld	de,txt_id_undef
	jp	error
err_bad_src_drv:
	ld	de,txt_bad_src_drv
	jp	error
err_no_matching_entry:
	ld	de,txt_no_matching_entry
	jp	error
err_no_src_entry:
	ld	de,txt_no_src_entry
	jp	error
err_too_many_entries:
	ld	de,txt_too_many_entries
	jp	error
err_file_exists:
	ld	de,txt_file_exists
	jp	error
err_dest_dir_full:
	ld	de,txt_dest_dir_full
	jp	error
err_cant_close:
	ld	de,txt_cant_close
	jp	error
err_bad_entry:
	ld	de,txt_bad_entry
	jp	error
err_disk_full:
	ld	de,txt_disk_full
	jp	error
err_src_empty:
	ld	de,txt_src_empty
	jp	error
err_src_too_big:
	ld	de,txt_src_too_big
	jp	error
error:
	push	de
	print	txt_err_on
	pop	de
	bdos_call	prtstr
	print	txt_err_off
	jp	quit
;----------------------------------------------------------
;
; bad messages
;
txt_err_on:
	defb	bell,cr,lf
	defm	'*** Fehler:'
	defb	cr,lf,'$'
txt_err_off:
	defb	cr,lf,cr,lf,'$'
txt_no_mem:
	defm	'Zuwenig Speicher !$'
txt_no_src_name:
	defm	'Welches File soll kopiert werden ?'
	defb	cr,lf
	defb	cr,lf
txt_bad_parm:
	defm	'Schlechte Parameter.'
	defb	cr,lf
	defm	'Aufruf mit: CM <lw:>filename<.ext> lw:<filename><.ext>'
	defb	cr,lf,'$'
txt_read_err:
	defm	'Lesefehler$'
txt_no_dest_drv:
	defm	'Nummer des MSDOS-Laufwerks nicht angegeben'
	defb	cr,lf
	defm	'oder der MSDOS-Filename ist fehlerhaft.$'
txt_bad_dest_drv:
	defm	'Fehlerhafte MSDOS-Laufwerksbezeichnung.'
	defb	cr,lf
	defm	'Bitte Laufwerk mit ''A:'' bis ''P:'' angeben.$'
txt_bad_src_drv:
	defm	'Fehlerhafte CP/M-Laufwerksbezeichnung.'
	defb	cr,lf
	defm	'Bitte Laufwerk mit ''A:'' bis ''P:'' angeben.$'
txt_dest_not_ready:
	defm	'Keine Diskette im angegebenen Laufwerk.$'
txt_id_undef:
	defm	'MS-DOS Format unbekannt.$'
txt_no_matching_entry:
	defm	'Keine passende CP/M-Datei gefunden.$'
txt_no_src_entry:
	defm	'Kann CP/M-Datei nicht |ffnen.$'
txt_too_many_entries:
	defm	'Zu viele passende CP/M-Dateien gefunden.$'
txt_dest_dir_full:
	defm	'Inhaltsverzeichnis der MSDOS-Diskette voll.$'
txt_file_exists:
	defm	'Eine Datei unter diesem Namen existiert bereits.$'
txt_write_err:
	defm	'Fehler beim Schreiben der Ziel-Datei.$'
txt_cant_close:
	defm	'Fehler beim Schlie~en der Ziel-Datei.$'
txt_bad_entry:
	defm	'MSDOS-Directory fehlerhaft !$'
txt_disk_full:
	defm	'MSDOS-Diskette ist voll.$'
txt_src_empty:
	defm	'CP/M-Datei ist leer !$'
txt_src_too_big:
	defm	'CP/M-Datei ist zu gro~ zum kopieren.$'
;
; good messages
;
heading:	defb	cr,lf
		defm	'Datei}bertragung CP/M 2.2 => MS-DOS'
		defb	cr,lf
		defm	'Copyright (c) by Klaus K{mpf Softwareentwicklung 1986'
		defb	cr,lf,cr,lf,'$'
footing:	defb	'$'
eol:		defb	cr,lf,'$'
;
txt_file_found:	defm	'Quell-Datei gefunden: '
txt_name_found:	defm	'FILENAME.EXT'
		defb	cr,lf,'$'
txt_start_copying:
		defb	cr,lf
		defm	'Kopiere   '
from_drive:	defm	'X:'
from_filename:	defm	'NNNNNNNN.EEE'
		defm	'  nach  '
to_drive:	defm	'X:'
to_filename:	defm	'NNNNNNNN.EXT'
		defb	cr,lf,cr,lf,'$'
opening_dest:	defm	'Ziel-Datei wird angelegt.'
		defb	cr,lf,'$'
closing_dest:	defb	cr,lf,cr,lf
		defm	'Ziel-Datei wird geschlossen.'
		defb	cr,lf,'$'
reading_source:	defb	cr,'    Lese Sektor '
numrea:		defb	'    H',cr,'$'
writing_dest:	defb	cr,'Schreibe Sektor '
numwrt:		defb	'    H',cr,'$'
;
	defm	'<*****FROM*****>'
@from_fcb:	defb	0
@from_name:	defm	'FROMNAMEEXT'
		defs	24,0
	defm	'<****SOURCE****>'
@src_drive:	defb	0
@src_code:	defb	0
@src_fcb:	defb	0
@src_name:	defm	'SRCENAMEEXT'
		defs	24,0
;
	defm	'<******TO******>'
@to_name:	defm	'TONAMEMEEXT'
	defm	'<*****DEST*****>'
@dest_drive:	defb	0
@dest_code:	defb	0
@dest_name:	defm	'DESTNAMEEXT'
;
?@found_entry:	defw	0
?@entry:	defw	0
@entry_#:	defb	0
@joker?:	defb	0			;Joker in Filename ?
@found?:	defb	0			;At least 1 file found ?
@first?:	defb	0
@table_entries:	defb	0
?@entry_table:	defw	@entry_table
;
@eof_pos:	defb	0
@data_full?:	defb	0
@data_len:	defb	0
@length:	defw	0
@read_len:	defw	0
@write_len:	defw	0
;
;
;----------------------------------------------------------
;
@set_cluster:	defw	0
@set_value:	defw	0
@first_cluster:	defw	0
@free_cluster:	defw	0
@cluster:	defw	0
@sec_in_clus:	defb	0
@lsn:		defw	0
@rec_in_sec:	defb	0
@relrec:	defw	0
?@cluster_area:	defw	@data_area
?@lsn_area:	defw	@data_area
?@record_area:	defw	@data_area
?@data_area:	defw	@data_area
?@dma:		defw	@data_area
@track:		defw	0
@record:	defw	0
	defm	'<***ENTRIES:***>'
@entry_table:
	defs	32*11,0				;max 32 files
	defm	'<*****FAT:*****>'
@fat_area:
	defs	7*512,0
	defm	'<*****DIR:*****>'
@dir_area:
	defs	112*32,0
	defm	'<*****DATA*****>'
@data_area:
