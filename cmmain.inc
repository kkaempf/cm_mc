	.printl Including CMMAIN.INC
;
	print	heading
	ld	hl,(?boot+1)			;HL -> ?bios+3
	ld	l,0
	ld	(?@bios),hl
;
	ld	hl,(?bdos+1)			;HL -> BDOS start
	ld	de,@data_area+32768		;do we have a 32k buffer ?
	or	a
	sbc	hl,de
	jp	c,err_no_mem			;N: No memory!
	ld	a,(@buffer)
	or	a
	jp	z,err_bad_parm
	call	get_from_name			;from fcb at 5C
	call	get_to_name			;from fcb at 6C
	call	get_id
	call	get_dest_fat
	call	get_dest_dir
	xor	a
	ld	(@found?),a			;no found yet
	call	reset_all_parms
	call	reset_system
	call	search_src_entries
	ld	hl,@entry_table
	ld	(?@entry_table),hl
;
copy_next_file:
	call	next_src_entry
	jr	nz,no_more_files
	call	reset_system
	call	open_source
	call	compute_length			;set @length
	call	make_dest
	call	copy_files
	call	close_dest
	jr	copy_next_file
no_more_files:
	call	select_dest
	call	put_dest_fat
	call	put_dest_dir
	print	footing
quit:
	call	reset_system
	jp	?boot				;Y: quit normal
;
;----------------------------------------------------------
;
; copy_files
; I: IX -> dest entry
;    src_fcb set
; O: source copied to dest
;
copy_files:
	call	start_copying			;show what we're copying
	ld	hl,-1
	ld	(@read_len),hl			;nothing read yet
	ld	(@write_len),hl			;nothing written either
	ld	a,(iy+.rps)
	dec	a
	ld	(@rec_in_sec),a
	ld	a,(iy+.spc)
	dec	a
	ld	(@sec_in_clus),a
	xor	a
	ld	(@first?),a
	ld	hl,2
	ld	(@cluster),hl
	call	reset_data			;reset data area
	ld	bc,(@length)			;BC =  # of records
copy_loop:
	push	bc				;save length
	call	read_src_rec			;show & read
	ld	hl,@data_len
	inc	(hl)				;buffer full ?
	call	z,write_data_area		;write data if buffer full
	pop	bc
	dec	bc
	ld	a,b
	or	c
	jr	nz,copy_loop
	ld	a,(@data_full?)
	bit	0,a				;Unwritten data in buffer ?
	call	nz,write_data_area		;Y: flush buffer
	ret
;----------------------------------------------------------
;
; Write complete data area
; I: @data_area filled
; O: @data_area written
;
write_data_area:
	call	reset_system
	call	select_dest
	ld	hl,@data_area
	ld	a,(@data_len)
	ld	b,a
write_data_loop:
	push	bc,hl
	ld	(?@dma),hl			;set DMA
	call	break?
	ld	hl,(@write_len)
	inc	hl
	ld	(@write_len),hl
	ld	de,numwrt
	call	hexasc
	print	writing_dest
	call	write_data_record
	pop	hl,bc
	or	a
	jp	nz,err_write_err
	ld	de,128
	add	hl,de
	djnz	write_data_loop
	ld	hl,@data_full?
	res	0,(hl)				;FLAG RESET -> buffer clear
	call	reset_data
	call	reset_system
	ret
;
;
; Write next record to free cluster
; Allocate cluster
; Update FAT
;
; I: IX -> entry
;    ?@dma set
;    @relrec set
;    @rec_in_sec set
;    @sec_in_clus set
;    @lsn set
;    @cluster set
; O: same values set
;    evtl. in new cluster
;
write_data_record:
	ld	a,(@rec_in_sec)
	inc	a
	cp	(iy+.rps)			;sector  full?
	jr	c,write_rec2sec			;N: write rec to same sec
	ld	hl,(@lsn)
	inc	hl
	ld	(@lsn),hl
	ld	a,(@sec_in_clus)
	inc	a
	cp	(iy+.spc)
	jr	c,write_sec2clus
	call	allocate_cluster		;Y: set @cluster
	call	compute_lsn			;@cluster -> @lsn
	xor	a
write_sec2clus:
	ld	(@sec_in_clus),a
	call	compute_relrec			;@lsn -> @relrec
	xor	a				;start new lsn
write_rec2sec:
	ld	(@rec_in_sec),a
	call	write_dest_rec			;write to ?@dma,@relrec
	ld	hl,(@relrec)
	inc	hl
	ld	(@relrec),hl			;save new relrec
	ret
;
;==========================================================
; HANDLING
;
;
; Close Destinationfile
; I: ?@entry -> entry
; O: Entry closed (cluster & eof set)
;
close_dest:
	print	closing_dest
	ld	ix,(?@entry)
	ld	hl,(@first_cluster)
	ld	(ix+26),l
	ld	(ix+27),h
	ld	hl,(@length)			;hl = # of records
	xor	a
	srl	h
	rr	l
	rra
	ld	(ix+28),a
	ld	(ix+29),l
	ld	(ix+30),h
	ld	(ix+31),0
	ret
;
;
; Allocate new/next cluster
; I: @fat_pointer set
;    @cluster set (full !)
; O: @cluster set (empty !)
;
allocate_cluster:
	call	get_free_cluster
	ld	a,(@first?)
	or	a
	jr	z,set_first_cluster
	ld	de,(@free_cluster)
	ld	hl,(@cluster)
	call	set_to_cluster
set_alloc_cluster:
	ld	hl,(@free_cluster)
	ld	(@cluster),hl
	ret
;
set_first_cluster:
	ld	a,0ffh
	ld	(@first?),a
	ld	hl,(?@entry)
	ld	de,26
	add	hl,de
	ld	de,(@free_cluster)
	ld	(@first_cluster),de
	ld	(hl),e
	inc	hl
	ld	(hl),d
	jr	set_alloc_cluster
;
;
; get free cluster
; I: IY -> ID Block
; O: @free_cluster set (new cluster)
;    free_cluster set to FFFh
;
get_free_cluster:
	ld	hl,(@cluster)			;starting from last cluster
free_clus_loop:
	push	hl
	ld	de,@fat_area
	ld	c,l
	ld	b,h
	srl	b
	rr	c
	push	af
	add	hl,bc
	add	hl,de
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	pop	af
	jr	c,get_odd_cluster
	ld	a,d				;even: use bits 0..11 0xxx
	and	0000_1111b
	ld	d,a
	jr	check_cluster
get_odd_cluster:
	rept	4				;odd: use bits 4..15 xxx0
	srl	d
	rr	e
	endm
check_cluster:
	pop	hl
	ld	a,e
	or	d
	jr	z,free_clus_found
	inc	hl
	push	hl
	ld	e,(iy+.clusters)
	ld	d,(iy+.clusters+1)
	or	a
	sbc	hl,de
	pop	hl
	jr	c,free_clus_loop
	jp	err_disk_full
free_clus_found:
	ld	(@free_cluster),hl
	ld	de,0fffh
	call	set_to_cluster			;set de to cluster hl
	ret
;
;
; set value to cluster in fat
; I: HL = cluster #
;    DE = value (FFF or link to next cluster)
;
set_to_cluster:
	ld	(@set_cluster),hl
	ld	(@set_value),de
	ld	de,@fat_area
	ld	c,l
	ld	b,h
	srl	b
	rr	c
	push	af
	add	hl,bc
	add	hl,de
	ld	de,(@set_value)
	pop	af
	jr	c,set_odd_cluster
	ld	(hl),e
	inc	hl
	ld	a,(hl)
	and	1111_0000b
	or	d
	ld	(hl),a
	ret
set_odd_cluster:
	rept	4
	sla	e
	rl	d
	endm
	ld	a,(hl)
	and	0000_1111b
	or	e
	ld	(hl),a
	inc	hl
	ld	(hl),d
	ret
;
;
; Make destination
; I: @src_name set
; O: @dest_name set
;    IX -> entry
;
make_dest:
	call	set_dest_name			;set @dest_name
	call	dest_existant?			;check if existant
	call	search_free_entry		;set ?@entry
	call	init_entry			;initialize entry
	ret
;
;
;
; Search next free entry
; I: -
; O: HL -> free entry
;    Exit if no free entry found
;
search_free_entry:
	ld	b,(iy+.entries)			;B = # of entries
	ld	hl,@dir_area			;hl -> @dir_area
search_free_loop:
	ld	a,(hl)
	or	a
	jr	z,free_entry_found
	cp	0e5h
	jr	z,free_entry_found
	ld	de,32
	add	hl,de
	djnz	search_free_loop
	jp	err_dest_dir_full
free_entry_found:
	ret
;
;
; Set destination name
; I: @to_name set
;    @src_name set
; O: @dest_name set
;
set_dest_name:
	ld	de,@dest_name			;de -> dest name
	ld	hl,@to_name			;hl -> to name
	ld	bc,@src_name			;bc -> src name
	ld	a,11
set_copy_names:
	push	af
	ld	a,(hl)				;get 'to' char
	cp	'?'				;joker ?
	jr	nz,leave_dest_char		;N: leave char
	ld	a,(bc)				;Y: get 'src' char
leave_dest_char:
	ld	(de),a				;put entry char
	inc	hl
	inc	de
	inc	bc
	pop	af
	dec	a
	jr	nz,set_copy_names
	ret
;
;
; Init entry
; Setup entry 
; I: HL -> free entry
;    @src_name set
;    @to_name set
; O: IX -> opened entry
;    @dest_name set
;    ?@entry set
;
init_entry:
	ld	(?@entry),hl
	push	hl
	ld	b,32
init_clear_loop:
	ld	(hl),0
	inc	hl
	djnz	init_clear_loop
	pop	de
	push	de				;de -> entry
	ld	hl,@dest_name
	ld	bc,11
	ldir					;set entry name
	pop	ix				;ix -> entry
	ld	(ix+24),0010_0001b		;set date 1.1.1980
	ret
;----------------------------------------------------------
; DATA I/O
;
; Search matching File
; I: @from_fcb set
; O: @entry_table set
;    @table_entries set
;
search_src_entries:
	ld	hl,@entry_table
	ld	(?@entry_table),hl
	xor	a
	ld	(@table_entries),a
search_src_entries_loop:
	call	get_matching_entry
	ret	z				;finished if no more found
	ld	a,(@table_entries)
	inc	a
	cp	33
	jp	z,err_too_many_entries
	ld	(@table_entries),a
						;hl -> name
	ld	de,(?@entry_table)
	ld	bc,11
	ldir					;move name to table
	ld	(?@entry_table),de
	jr	search_src_entries_loop
;
;
; Get next src entry
; I: @entry_table set
;    @table_entries set
; O: Z: @src_fcb set
;    NZ: no more entries
;
next_src_entry:
	ld	a,(@table_entries)
	sub	1
	ret	c
	ld	(@table_entries),a
	ld	hl,(?@entry_table)		;hl -> name of next entry
	ld	de,@src_name			;de -> name of source entry
	ld	b,11
copy_found_name:
	ld	a,(hl)
	and	7fh
	ld	(de),a
	inc	hl
	inc	de
	djnz	copy_found_name
	ld	(?@entry_table),hl
	ld	b,4+16+4
clear_found_fcb:
	ld	(de),0
	inc	de
	djnz	clear_found_fcb
	ld	a,(@from_fcb)
	ld	(@src_fcb),a
	xor	a
	ret
;
;
; get first/next matching entry
; I: @from_fcb set
; O: NZ: HL -> matching name
;    Z: none found
;
get_matching_entry:
	bdos_call	setdma,@buffer
	ld	a,(@found?)			;first search ?
	or	a
	jr	nz,get_next_matching
	bdos_call	first,@from_fcb
	cp	0ffh				;first found ?
	jp	z,err_no_matching_entry		;N: error
	jr	found_matching_entry
get_next_matching:
	bdos_call	next,@from_fcb
found_matching_entry:
	inc	a
	ret	z				;ret if none found
	ld	hl,@buffer+1-32
	ld	de,32
match_entry_loop:
	add	hl,de
	dec	a
	jr	nz,match_entry_loop		;hl -> matching name
	ld	a,0ffh
	ld	(@found?),a			;at least one found
	or	a				;set NZ
	ret
;
;
; Open Source file
; I: @source_fcb set
; O: @source_fcb opened or Error
;
open_source:
	bdos_call	open,@src_fcb
	inc	a
	jp	z,err_no_src_entry
	ret
;
;
; Compute length of Sourcefile
; I: @src_fcb opened
; O: @length set to # of records
;
compute_length:
	bdos_call	getsiz,@src_fcb
	ld	a,(@src_fcb+35)
	or	a
	jp	nz,err_src_too_big
	ld	hl,(@src_fcb+33)		;hl = r0/r1
	ld	(@length),hl
	ld	a,h
	or	l
	jp	z,err_src_empty
	ld	de,#maxlen
	or	a
	sbc	hl,de
	jp	nc,err_src_too_big
	ret
;
;
; Read source record
; I: @src_fcb set
;    ?@data_area set
; O: next record read
;    @src_fcb & ?@data_area incremented
;
read_src_rec:
	ld	de,(?@data_area)
	push	de
	bdos_call	setdma
	bdos_call	redseq,@src_fcb
	or	a
	jp	nz,err_read_err
	ld	hl,(@read_len)
	inc	hl
	ld	(@read_len),hl
	ld	de,numrea
	call	hexasc
	print	reading_source
	call	break?
	pop	hl
	ld	de,128
	add	hl,de
	ld	(?@data_area),hl
	ld	hl,@data_full?
	set	0,(hl)
	ret
;
;
; Read destination record
; I: HL = relrec
;    ?@dma set
; O: HL = relrec
;    record read
;
read_dest_rec:
	push	hl
	call	compute_trkrec
	ld	bc,(?@dma)
	bios_call	b_setdma
	ld	bc,(@track)
	bios_call	b_settrk
	ld	bc,(@record)
	bios_call	b_setsec
	bios_call	b_read
	pop	hl
	or	a
	ret	z
	jp	err_read_err
;
;
; Write destination record
; I: @relrec set
;    ?@dma set
; O: HL = relrec
;    record written
;
write_dest_rec:
	call	compute_trkrec
	ld	bc,(?@dma)
	bios_call	b_setdma
	ld	bc,(@track)
	bios_call	b_settrk
	ld	bc,(@record)
	bios_call	b_setsec
	bios_call	b_write
	or	a
	ret	z
	jp	err_write_err
;
;----------------------------------------------------------
;
;
; Dest file existant ?
; I: @dest_name set
; O: Exit if existant
;    else Z set
;
dest_existant?:
	ld	hl,@dir_area
	ld	b,(iy+.entries)
dest_exist_loop:
	push	hl,bc
	call	compare_dest
	pop	bc,hl
	jp	z,err_file_exists
	ld	de,32
	add	hl,de
	djnz	dest_exist_loop
	ret
;
compare_dest:
	ld	de,@dest_name
	ld	b,11
file_compare:
	ld	a,(de)
	cp	(hl)
	ret	nz
	inc	de
	inc	hl
	djnz	file_compare
	xor	a
	ret
