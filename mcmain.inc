	.printl Including MCMAIN.INC
;
	print	heading
;
	call	get_parms
	call	get_from_name
	call	get_to_name
	call	get_id				;IY -> ID Block
	call	get_src_dir			;get directory
	call	get_src_fat			;get FAT
	xor	a				;no match yet
	ld	(@found?),a
	ld	(@entry_#),a
	ld	hl,@dir_area			;hl -> 1st entry
	ld	(?@next_entry),hl
;
copy_next_file:
	call	reset_all_data
	call	search_src_entry
	jr	nz,no_more_src			;no match ? Y: quit
	push	hl
	pop	ix				;IX -> entry Pointer
	call	make_dest
	call	open_source			;IX -> FAT
	call	copy_files
	call	close_dest
	jr	copy_next_file
no_more_src:
	ld	a,(@found?)
	or	a				;source found ?
	jp	z,err_no_src_entry		;N: quit with error
quit:
	call	reset_system
	jp	?boot				;Y: quit normal
;
;----------------------------------------------------------
;
; copy_files
; I: @cluster set (1st cluster)
;    @src_len set
;    @dest_fcb set
; O: source copied to dest
;
copy_files:
	call	start_copying			;show what we're copying
	ld	hl,-1
	ld	(@read_len),hl			;nothing read yet
	ld	(@write_len),hl			;nothing written either
	ld	a,(iy+.rps)
	dec	a
	ld	(@rec_in_sec),a
	ld	a,(iy+.spc)
	dec	a
	ld	(@sec_in_clus),a
	call	reset_data			;reset data area
	call	select_src
	ld	hl,(@src_len)
copy_loop:
	push	hl				;save len
	call	read_data_record		;show & read
	ld	hl,@data_len
	inc	(hl)				;buffer full ?
	call	z,write_data_area		;write data if buffer full
	pop	hl
	dec	hl
	ld	a,h
	or	l
	jr	nz,copy_loop
	ret
;----------------------------------------------------------
;
; Read Data record to data_area & show reading
; I: @rec_in_sec set
;    @sec_in_clus set
;    @cluster set
;    ?@data_area set
; O: (?@data_area) -> record read
;
read_data_record:
	call	break?
	call	select_src
	ld	hl,(@read_len)
	inc	hl
	ld	(@read_len),hl
	ld	de,numrea
	call	hexasc				;show reading
	print	reading_source
	ld	a,(@rec_in_sec)
	inc	a
	ld	(@rec_in_sec),a
	cp	(iy+.rps)			;next sector reached ?
	jr	nz,next_record			;N: read next record
	xor	a
	ld	(@rec_in_sec),a
	ld	a,(@sec_in_clus)
	inc	a
	ld	(@sec_in_clus),a
	cp	(iy+.spc)			;next cluster reached ?
	jr	nz,next_sector
	xor	a
	ld	(@sec_in_clus),a
	call	next_cluster
	call	compute_lsn
	call	compute_relrec
	jr	next_data	
next_sector:
	ld	hl,(@lsn)
	inc	hl
	ld	(@lsn),hl
	call	compute_relrec
	jr	next_data
next_record:
	ld	hl,(@relrec)
	inc	hl
	ld	(@relrec),hl
next_data:
	ld	hl,(?@data_area)
	ld	(?@record_area),hl
	call	read_record			;read record
	ld	hl,(?@data_area)
	ld	de,128
	add	hl,de
	ld	(?@data_area),hl
	ld	hl,@data_full?
	set	0,(hl)				;SET FLAG -> data unwritten
	ret
;
;
; Write data_area by @data_len
; I: @data_area filled
; O: @data_area written to @dest_fcb
;
write_data_area:
	call	reset_system
	ld	hl,@data_area
	ld	a,(@data_len)
	ld	b,a
write_data_loop:
	push	bc,hl
	ld	(?@data_area),hl
	call	break?
	ld	hl,(@write_len)
	inc	hl
	ld	(@write_len),hl
	ld	de,numwrt
	call	hexasc
	print	writing_dest
	ld	de,(?@data_area)
	bdos_call	setdma
	bdos_call	wrtseq,@dest_fcb
	pop	hl,bc
	or	a
	jp	nz,err_write_err
	ld	de,128
	add	hl,de
	djnz	write_data_loop
	ld	hl,@data_full?
	res	0,(hl)				;FLAG RESET -> buffer clear
	jp	reset_data
;----------------------------------------------------------
;
; Finished with copying
;
close_dest:
	call	reset_system
	ld	a,(@eof_pos)			;EOF-Byte
	or	a
	jr	nz,eos_pos_nz
	ld	a,128
eos_pos_nz:
	ld	b,0
	ld	c,a
	ld	hl,(?@data_area)		;hl -> NEXT record
	ld	de,@data_area
	or	a
	sbc	hl,de				;Buffer just written ?
	ld	hl,(?@data_area)
	jr	z,close_no_add			;Y:no add
	ld	de,-128
	add	hl,de				;hl -> LAST record
close_no_add:
	add	hl,bc				;hl -> EOF Byte
	ld	(hl),ctrlz			;EOF-Markierung f}r CP/M
	ld	hl,@data_full?
	bit	0,(hl)
	call	nz,write_data_area		;write rest
	print	closing_dest			;Ziel-Datei schlie~en
	call	reset_system
	bdos_call	close,@dest_fcb
	inc	a
	jp	z,err_cant_close
	ret
;----------------------------------------------------------
;
;
; get next cluster
; I: @next_cluster set (old cluster)
; O: @cluster set (old cluster)
;    @next_cluster set (new cluster)
;
next_cluster:
	ld	hl,(@next_cluster)
	ld	(@cluster),hl
	ld	de,@fat_area
	ld	c,l
	ld	b,h
	srl	b
	rr	c
	push	af
	add	hl,bc
	add	hl,de
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	pop	af
	jr	c,odd_cluster
	ld	a,d
	and	0000_1111b
	ld	d,a
	jr	set_new_cluster
odd_cluster:
	rept	4
	srl	d
	rr	e
	endm
set_new_cluster:
	ld	(@next_cluster),de			;save next cluster
	ret
;---------------------------------------------------------
;
; Search source Entry
; I: @from_name set
;    @to_name set
; O: NZ : no (more) match
;    Z:   file found, dest_name set
;    HL -> found entry (if found)
;    @src_name set
;    @dest_name set
;
search_src_entry:
	ld	a,(@entry_#)
	inc	a
	ld	(@entry_#),a
	dec	a
	cp	(iy+.entries)			;max entries reached ?
	jr	nc,entry#_too_large		;Y: ret with NZ
	ld	hl,(?@next_entry)		;HL -> start of dir (1st srch)
						;   -> next entry (next srch)
	push	hl
	ld	a,(hl)
	or	a
	jr	z,search_next_entry
	cp	0e5h
	jr	z,search_next_entry
	ld	de,@from_name
	ld	b,11
compare_loop:
	ld	a,(de)
	cp	'?'				;joker ?
	jr	z,its_joker
	cp	(hl)				;same char
	jr	nz,search_next_entry
its_joker:
	inc	de
	inc	hl
	djnz	compare_loop
	ld	a,(hl)
	bit	3,a				;volume found ?
	jr	nz,search_next_entry		;Y: next
	bit	4,a				;dir found ?
	jr	nz,search_next_entry		;Y: next
	jr	entry_found
;
search_next_entry:
	pop	hl
	ld	de,32
	add	hl,de
	ld	(?@next_entry),hl		;set next entry addr
	jr	search_src_entry		;next entry
;
entry#_too_large:
	ld	a,0ffh
	or	a
	ret
;
entry_found:
	pop	hl
	push	hl
	ld	de,32
	add	hl,de
	ld	(?@next_entry),hl
	pop	hl
	push	hl				;hl -> entry
	ld	de,@src_name
	ld	bc,11
	ldir					;set @src_name
	pop	hl
	push	hl
	ld	de,txt_name_found
	ld	bc,8
	ldir
	inc	de
	ld	bc,3
	ldir
	print	txt_file_found			;show found name
	ld	a,0ffh
	ld	(@found?),a			;mark found !
	ld	de,@dest_name			;de -> dest name
	ld	hl,@to_name			;hl -> to name
	ld	bc,@src_name			;bc -> src name
	ld	a,11
init_copy_names:
	push	af
	ld	a,(hl)				;get 'to' char
	cp	'?'				;joker ?
	jr	nz,leave_dest_char		;N: leave char
	ld	a,(bc)				;Y: get 'src' char
leave_dest_char:
	ld	(de),a				;put dest char
	inc	hl
	inc	de
	inc	bc
	pop	af
	dec	a
	jr	nz,init_copy_names
	call	reset_system
	pop	hl				;restore entry addr
	ld	(?@found_entry),hl
	xor	a
	ret
;----------------------------------------------------------
;
; Create destination
; I: @dest_fcb set for open
; O: @dest_fcb set after open
;
make_dest:
	ld	a,(@dest_fcb)
	push	af
	ld	hl,@dest_fcb+12
	ld	b,24
make_dest_clear:
	ld	(hl),0
	inc	hl
	djnz	make_dest_clear
	call	reset_system
	bdos_call	open,@dest_fcb		;CP/M-Ziel-Datei |ffnen
	inc	a
	call	nz,file_exists
crea1:						;Datei wird angelegt
	pop	af
	ld	(@dest_fcb),a
	bdos_call	make,@dest_fcb
	inc	a
	jp	z,err_dest_dir_full
	ret
;
;
; Open source entry
; I: IX -> source entry
; O: @src_len set
;    @eof_pos set
;    @cluster set (1st cluster)
;    @next_cluster set
;
open_source:
	ld	l,(ix+28)
	ld	h,(ix+29)
	ld	c,(ix+30)
	ld	b,(ix+31)			;bchl = source length
	ld	a,b
	or	a
	jp	nz,err_src_too_big
	ld	a,c
	cp	128
	jp	nc,err_src_too_big
	ld	a,l
	ld	l,h
	ld	h,c
	rlca					;a,7 -> cy
	rl	l
	rl	h				;hl = source len in recs
	ld	a,(ix+28)
	and	0111_1111b
	jr	z,set_src_len
	inc	hl
set_src_len:
	ld	(@src_len),hl
	ld	(@eof_pos),a
	ld	l,(ix+26)
	ld	h,(ix+27)			;hl = 1st cluster
	ld	(@next_cluster),hl
	ret
