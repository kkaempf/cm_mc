# CP/M <-> MSDOS copy program

This software is for 'my' Genie III/IIIs CP/M 2.2 to copy files
between CP/M and MS-DOS (360k / 720k disks).

See MSDOSCNV.DOC (Wordstar, german) for instructions.
