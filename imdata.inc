	.printl Including IMDATA.INC
;
; Error Handling
;
err_no_mem:
	ld	de,txt_no_mem
	jp	error
err_bad_parm:
	ld	de,txt_bad_parm			;"Schlechte Parameter."
	jp	error
err_read_err:
	ld	de,txt_read_err
	jp	error
err_no_src_drv:
	ld	de,txt_no_src_drv
	jp	error
err_bad_src_drv:
	ld	de,txt_bad_src_drv
	jp	error
err_src_not_ready:
	ld	de,txt_src_not_ready
	jp	error
err_read_error:
	ld	de,txt_read_error
	jp	error
err_id_undef:
	ld	de,txt_id_undef
	jp	error
error:
	push	de
	print	txt_err_on
	pop	de
	bdos_call	prtstr
	print	txt_err_off
	jp	quit
;----------------------------------------------------------
;
; bad messages
;
txt_err_on:
	defb	bell,cr,lf
	defm	'*** Fehler:'
	defb	cr,lf,'$'
txt_err_off:
	defb	cr,lf,cr,lf,'$'
txt_no_mem:
	defm	'Zuwenig Speicher !$'
txt_bad_parm:
	defm	'Schlechte Parameter.'
	defb	cr,lf
	defm	'Aufruf mit: IM lw:'
	defb	cr,lf,'$'
txt_read_err:
	defm	'Lesefehler$'
txt_no_src_drv:
	defm	'Nummer des MSDOS-Laufwerks nicht angegeben'
	defb	cr,lf
	defm	'oder der MSDOS-Filename ist fehlerhaft.$'
txt_bad_src_drv:
	defm	'Fehlerhafte MSDOS-Laufwerksbezeichnung.'
	defb	cr,lf
	defm	'Bitte Laufwerk mit ''A:'' bis ''P:'' angeben.$'
txt_src_not_ready:
	defm	'Keine Diskette im angegebenen Laufwerk.$'
txt_read_error:
	defm	'Sektor defekt.$'
txt_id_undef:
	defm	'MS-DOS Format unbekannt.$'
;
; good messages
;
heading:	defb	cr,lf
		defm	'MSDOS-Directory unter CP/M 2.2'
		defb	cr,lf
		defm	'Copyright (c) by Klaus K{mpf Softwareentwicklung 1986'
		defb	cr,lf,cr,lf,'$'
footing:
		defb	'$'
;
volume:		defm	'Kennsatz ist $'
no_volume:	defm	'unbekannt'
		defb	cr,lf,'$'
@volume_label:	defm	'           '
		defb	cr,lf,'$'
no_files:	defm	'Keine Dateien vorhanden.'
		defb	cr,lf,'$'
month_table:	defm	'Jan.Feb.M{rzApr.Mai JuniJuliAug.Sep.Okt.Nov.Dez.'
txt_usable:	defm	'Diskgr|~e: '
@usable_#:	defm	'00000000 Bytes$'
txt_used:	defm	' davon '
@used_#:	defm	'00000000 Bytes belegt$'
txt_remaining:	defm	' und '
@remaining_#:	defm	'00000000 Bytes frei'
		defb	cr,lf,'$'
@filename:	defm	'FILENAME.EXT  $'
@attributes:	defm	'        $'
attrib_vals:	defm	'RHSVDA  '
attrib_dir:	defm	'<DIR>   $'
txt_filesize:
@filesize:	defm	'000000  $'
txt_date:
@day:		defm	'DD. '
@month:		defm	'MMMM '
@year:		defm	'YYYY  $'
txt_time:
@hour:		defm	'HH:'
@minute:	defm	'MM:'
@second:	defm	'SS'
		defb	cr,lf,'$'
txt_sizes:	defb	cr,lf,' ',' '
@files_#:	defm	'000 Datei$'
more_files:	defm	'en'
eol:		defb	cr,lf,'$'
;
;----------------------------------------------------------
;
@src_drive:	defb	0
@src_code:	defb	0
@zero?:		defb	0
@files:		defb	0			;# of files
@used:		defw	0,0			;# of used bytes
@lsn:		defw	0
?@dir_area:	defw	@dir_area
@relrec:	defw	0
?@dma:		defw	@dir_area
@track:		defw	0
@record:	defw	0
	defm	'<***DATA***>'
@dir_area:
