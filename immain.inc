	.printl Including IMMAIN.INC
;
	print	heading
	ld	hl,(?boot+1)			;HL -> ?bios+3
	ld	l,0
	ld	(?@bios),hl
	ld	hl,(?bdos+1)			;HL -> BDOS start
	ld	de,@dir_area+32768		;do we have a 32k buffer ?
	or	a
	sbc	hl,de
	jp	c,err_no_mem			;N: No memory!
	call	get_parms
	call	get_id				;IY -> ID Block
	call	read_dir			;@dir_area = dir
	call	show_dir			;console = files
quit:
	print	footing
	jp	?boot
;----------------------------------------------------------
;
; Read complete directory to @dir_area
; I: IY -> ID Table
; O: @dir_area filled
;
read_dir:
	ld	hl,@dir_area			;hl -> data area
	ld	e,(iy+.dir)
	ld	d,(iy+.dir+1)			;de = 1st dir lsn
	ld	b,(iy+.dirlen)			;B = # of dir lsns
read_dir_loop:
	push	bc,de,hl
	ld	(?@dir_area),hl
	ld	(@lsn),de
	call	read_lsn
	pop	hl
	ld	e,(iy+.secsize)
	ld	d,(iy+.secsize+1)
	add	hl,de
	pop	de
	inc	de				;next lsn
	pop	bc
	djnz	read_dir_loop
	ret
;
;
; Show complete directory arrcording to parameters (?)
; I: @dir_area filled with directory
; O: directory given to conout
;
show_dir:
	call	show_volume
	call	show_files
	call	show_sizes
	ret
;
;----------------------------------------------------------
;
; Find & show volume label of directory
; I: @dir_area filled with directory
; O: volume label printed
;
show_volume:
	print	volume
	ld	ix,@dir_area			;IX -> data area
	ld	de,32				;de = entry length
	ld	b,(iy+.entries)
find_volume_label:
	ld	a,(ix+11)			;check flags
	bit	3,a				;Volume found ?
	jr	nz,found_volume
	add	ix,de
	djnz	find_volume_label
	print	no_volume
	print	eol
	ret
found_volume:
	push	ix
	pop	hl
	ld	de,@volume_label
	ld	bc,11
	ldir
	print	@volume_label
	print	eol
	ret
;
;
; Show all Files and attributes
; I: @dir_area filled
;    IY -> ID Block
; O: files printed
;
show_files:
	xor	a
	ld	(@files),a			;no files yet
	ld	hl,0
	ld	(@used),hl
	ld	(@used+2),hl			;no bytes used yet
	ld	b,(iy+.entries)
	ld	ix,@dir_area
show_files_loop:
	push	bc
	call	show_entry
	pop	bc
	ld	de,32
	add	ix,de
	djnz	show_files_loop
	ret
;
;
; Show # of files found, Bytes used, Bytes free
;
show_sizes:
	ld	a,(@files)
	or	a
	jr	z,no_file_found
	ld	hl,(@files)
	ld	h,0				;hl = #
	ld	de,@files_#
	ld	a,3
	call	number16 			;'print' hl to de
	print	txt_sizes
	ld	a,(@files)
	cp	1				;one file ?
	jr	z,one_file
	print	more_files
one_file:
	print	eol
	jr	show_disk_size
no_file_found:
	print	no_files
;
show_disk_size:
	print	eol
	ld	l,(iy+.spd)
	ld	h,(iy+.spd+1)			;hl = secs per disk
	ld	e,(iy+.data)
	ld	d,(iy+.data+1)			;de = 1st data lsn
	or	a
	sbc	hl,de				;hl = # of usable lsns
	ld	c,h
	ld	b,0
	ld	h,l
	ld	l,0				;bchl = lsns * 256
	srl	c
	rr	h
	rr	l				;bchl = lsns * 128
	srl	c
	rr	h
	rr	l				;bchl = lsns * 64
	ld	a,(iy+.rps)			;a = recs per sec 1/2/4/8
disk_size_loop:
	sla	l
	rl	h
	rl	c
	rl	b
	rrca
	jr	nc,disk_size_loop		;bchl = bytes per disk
	push	bc,hl
	ld	de,@usable_#
	ld	a,8
	call	number32			;print bchl to de
	print	txt_usable
	ld	hl,(@used)
	ld	bc,(@used+2)			;bchl = bytes used
	ld	de,@used_#
	ld	a,8
	call	number32
	print	txt_used
	pop	hl,bc				;bchl = bytes per disk
	ld	de,(@used)
	or	a
	sbc	hl,de
	ex	de,hl
	ld	l,c
	ld	h,b
	ld	bc,(@used+2)
	sbc	hl,bc
	ld	c,l
	ld	b,h
	ex	de,hl				;bchl = bytes remaining
	ld	de,@remaining_#
	ld	a,8
	call	number32
	print	txt_remaining
	ret
;----------------------------------------------------------
;
; Show single directory entry
; I: IX -> entry
; O: entry printed
;
show_entry:
	ld	a,(ix+0)			;1st byte
	or	a				;unused ?
	ret	z				;Y: ret
	cp	0e5h				;erased ?
	ret	z				;Y: ret
	ld	a,(ix+11)			;attribute
	bit	3,a				;volume ?
	ret	nz				;y: already printed
	ld	hl,@files
	inc	(hl)				;file found
	call	print_name			;Print name
	call	print_attributes		;Print attributes
	call	print_file_size			;Print file size
	call	print_date_time			;Print date & time
	ret
;
;
; Print filename and extension
; I: IX -> entry
; O: filename & extension printed
;
print_name:
	push	ix
	pop	hl
	ld	de,@filename
	ld	bc,8
	ldir
	inc	de
	ld	bc,3
	ldir
	print	@filename
	ret
;
;
; Print file attributes
; I: IX -> entry
; O: attributes printed
;
print_attributes:
	ld	a,(ix+11)
	bit	4,a				;dir ?
	jr	nz,dir_attrib
	ld	hl,@attributes
	ld	b,6
	ld	de,attrib_vals
	ld	c,a
attributes_loop:
	ld	a,(de)
	srl	c
	jr	c,attrib_found
	ld	a,'.'
attrib_found:
	ld	(hl),a
	inc	hl
	inc	de
	djnz	attributes_loop
	print	@attributes
	ret
dir_attrib:
	print	attrib_dir
	ret
;
;
; Print file size in bytes
; I: IX -> entry
; O: size printed
;
print_file_size:
	ld	l,(ix+28)
	ld	h,(ix+29)
	ld	c,(ix+30)
	ld	b,(ix+31)
	ld	a,(@used)
	add	a,l
	ld	(@used),a
	ld	a,(@used+1)
	adc	a,h
	ld	(@used+1),a
	ld	a,(@used+2)
	adc	a,c
	ld	(@used+2),a
	ld	a,(@used+3)
	adc	a,b
	ld	(@used+3),a
	ld	de,@filesize
	ld	a,6
	call	number32
	print	txt_filesize
	ret
;
;
; print file date and time
; I: IX -> entry
; O: date and time printed
;
print_date_time:
	ld	a,(ix+24)			;print day
	and	0001_1111b
	ld	l,a
	ld	de,@day
	ld	a,2
	call	number8
	ld	a,(ix+24)			;print month
	and	1110_0000b
	ld	b,(ix+25)
	srl	b
	rra
      4:rrca
	ld	hl,month_table-4
	ld	de,4
	ld	b,a
month_loop:
	add	hl,de
	djnz	month_loop
	ld	de,@month
	ld	bc,4
	ldir
	ld	de,1980				;print year
	ld	h,0
	ld	l,(ix+25)
	srl	l
	add	hl,de
	ld	de,@year
	ld	a,4
	call	number16
	print	txt_date
	ld	a,(ix+23)			;print hour
	and	1111_1000b
      3:rrca
	ld	l,a
	ld	h,0
	ld	bc,0
	
	ld	de,@hour
	ld	a,2
	call	number8y
	ld	b,(ix+23)			;print minute
	ld	a,(ix+22)
	and	1110_0000b
	srl	b
	rra
	srl	b
	rra
	srl	b
	rra
	rrca
	rrca
	ld	l,a
	ld	h,0
	ld	bc,0
	ld	de,@minute
	ld	a,2
	call	number8y
	ld	a,(ix+22)
	and	0001_1111b
	rlca
	ld	l,a
	ld	h,0
	ld	bc,0
	ld	de,@second
	ld	a,2
	call	number8y
	print	txt_time
	ret
;----------------------------------------------------------
;
; Read LSN to DMA
; I: IY -> ID Block
;    @lsn set
;    ?@dir_area set
; O: LSN read to DMA or Error
;
read_lsn:
	call	compute_relrec			;convert @lsn -> @relrec
	ld	hl,(?@dir_area)
	ld	(?@dma),hl
	ld	b,(iy+.rps)			;B = # of records per sec
read_lsn_loop:
	push	bc
	call	read_record
	pop	bc
	ld	hl,(@relrec)
	inc	hl
	ld	(@relrec),hl
	ld	hl,(?@dma)
	ld	de,128
	add	hl,de
	ld	(?@dma),hl
	djnz	read_lsn_loop
	ret
;
;
; Read single record
; I: IY: ID Block
;    ?@dma set
;    @relrec set
; O: ?@dma filled with record
;
read_record:
	call	compute_trkrec			;convert @relrec -> @trk,@sec
	ld	bc,(?@dma)
	bios_call	b_setdma
	ld	bc,(@track)
	bios_call	b_settrk
	ld	bc,(@record)
	bios_call	b_setsec
	bios_call	b_read
	or	a
	ret	z
	jp	err_read_error
;
