	.printl Including CMMISC.INC
;
; Get & check parameters 
; I: @buffer set
; O:
;
get_parms:
	ld	hl,(?boot+1)			;HL -> ?bios+3
	ld	l,0
	ld	(?@bios),hl
	ld	hl,(?bdos+1)			;HL -> BDOS start
	ld	de,@data_area+32768		;do we have a 32k buffer ?
	or	a
	sbc	hl,de
	jp	c,err_no_mem			;N: No memory!
	ld	a,(@buffer)			;L{nge des Befehls
	or	a
	jp	z,err_bad_parm			;Err if no Parms given
	ret
;
;
; Get ID Table address by evaluating ID-Byte at Boot+.boot_id
; I: -
; O: IY -> ID Table
;
get_id:
	call	reset_system
	call	select_dest
	ld	a,.boot_id
	ld	hl,#boot_record			;Try bootrecord 1st
	call	GetIDRecord
	jr	nc,evaluate_id
	ld	a,.fat_id
	ld	hl,#fat_record			;Then IBM-Fat
	call	GetIDRecord
	jr	nc,evaluate_id
	ld	a,.fat_id
	ld	hl,#jfat_record			;Then J-Format Fat
	call	GetIDRecord
evaluate_id:
	ld	hl,@id_table
	cp	(hl)
	jp	c,err_id_undef
	dec	hl
get_id_loop:
	inc	hl
	inc	hl
	inc	a
	jr	nz,get_id_loop			;HL -> id table pointer
	ld	a,(hl)
	inc	hl
	ld	h,(hl)
	ld	l,a				;HL -> ID table
	push	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	bdos_call	prtstr			;Show what we've found
	print	eol
	print	eol
	pop	iy
	ret
;
;
; get id_record (boot or fat or whatsoever)
; I: A  = id_offset in record
;    HL = record #
; O: nc, if id found > _MINID
;
GetIDRecord:
	ld	(@id_offset),a
	ld	(@relrec),hl
	ld	hl,@dir_area
	push	hl
	ld	(?@record_area),hl
	ld	iy,(@id_table+1)		;IY -> Dummy ID
	call	read_record			;Read @relrec to ?@dma
	pop	iy
	ld	a,(iy+<@id_offset: 0>)
	cp	_MINID
	ret
;
;
; Read complete directory to @dir_area
; I: IY -> ID Table
; O: @dir_area filled
;
get_dest_dir:
	ld	hl,@dir_area			;hl -> dir area
	ld	e,(iy+.dir)
	ld	d,(iy+.dir+1)			;de = 1st dir lsn
	ld	b,(iy+.dirlen)			;B = # of dir lsns
get_dir_loop:
	push	bc,de,hl
	ld	(?@lsn_area),hl
	ld	(@lsn),de
	call	read_lsn
	pop	hl
	ld	e,(iy+.secsize)
	ld	d,(iy+.secsize+1)
	add	hl,de
	pop	de
	inc	de				;next lsn
	pop	bc
	djnz	get_dir_loop
	ret
;
;
; Write complete directory from @dir_area
; I: IY -> ID Table
; O: @dir_area written
;
put_dest_dir:
	ld	hl,@dir_area			;hl -> dir area
	ld	e,(iy+.dir)
	ld	d,(iy+.dir+1)			;de = 1st dir lsn
	ld	b,(iy+.dirlen)			;B = # of dir lsns
put_dir_loop:
	push	bc,de,hl
	ld	(?@lsn_area),hl
	ld	(@lsn),de
	call	write_lsn
	pop	hl
	ld	e,(iy+.secsize)
	ld	d,(iy+.secsize+1)
	add	hl,de
	pop	de
	inc	de				;next lsn
	pop	bc
	djnz	put_dir_loop
	ret
;
;
; Read complete FAT to @fat_area
; I: IY -> ID Table
; O: @fat_area filled
;
get_dest_fat:
	ld	hl,@fat_area			;hl -> fat area
	ld	e,(iy+.fat)
	ld	d,(iy+.fat+1)			;de = 1st fat lsn
	ld	b,(iy+.fatlen)			;B = # of fat lsns
get_fat_loop:
	push	bc,de,hl
	ld	(?@lsn_area),hl
	ld	(@lsn),de
	call	read_lsn
	pop	hl
	ld	e,(iy+.secsize)
	ld	d,(iy+.secsize+1)
	add	hl,de
	pop	de
	inc	de				;next lsn
	pop	bc
	djnz	get_fat_loop
	ret
;
;
; Write complete FAT from @fat_area
; I: IY -> ID Table
; O: @fat_area written
;
put_dest_fat:
	ld	hl,@fat_area			;hl -> fat area
	ld	e,(iy+.fat)
	ld	d,(iy+.fat+1)			;de = 1st fat lsn
	ld	b,(iy+.fatlen)			;B = # of fat lsns
put_fat_loop:
	push	bc,de,hl
	ld	(?@lsn_area),hl
	ld	(@lsn),de
	call	write_lsn
	pop	hl
	ld	e,(iy+.secsize)
	ld	d,(iy+.secsize+1)
	add	hl,de
	pop	de
	inc	de				;next lsn
	pop	bc
	djnz	put_fat_loop
	ret
;
;----------------------------------------------------------
;
; Read LSN to DMA
; I: IY -> ID Block
;    @lsn set
;    ?@lsn_area set
; O: LSN read to DMA or Error
;
read_lsn:
	ld	hl,(?@lsn_area)
	ld	(?@record_area),hl
	call	compute_relrec			;convert @lsn -> @relrec
	ld	b,(iy+.rps)			;B = # of records per sec
read_lsn_loop:
	push	bc
	call	read_record
	pop	bc
	ld	hl,(@relrec)
	inc	hl
	ld	(@relrec),hl
	ld	hl,(?@record_area)
	ld	de,128
	add	hl,de
	ld	(?@record_area),hl
	djnz	read_lsn_loop
	ret
;
;
; Write LSN from DMA
; I: IY -> ID Block
;    @lsn set
;    ?@lsn_area set
; O: LSN written to DMA or Error
;
write_lsn:
	ld	hl,(?@lsn_area)
	ld	(?@record_area),hl
	call	compute_relrec			;convert @lsn -> @relrec
	ld	b,(iy+.rps)			;B = # of records per sec
write_lsn_loop:
	push	bc
	call	write_record
	pop	bc
	ld	hl,(@relrec)
	inc	hl
	ld	(@relrec),hl
	ld	hl,(?@record_area)
	ld	de,128
	add	hl,de
	ld	(?@record_area),hl
	djnz	write_lsn_loop
	ret
;
;
; Read single record
; I: IY: ID Block
;    ?@record_area set
;    @relrec set
; O: (?@record_area) filled with record
;
read_record:
	call	compute_trkrec			;convert @relrec -> @trk,@sec
	ld	bc,(?@record_area)
	bios_call	b_setdma
	ld	bc,(@track)
	bios_call	b_settrk
	ld	bc,(@record)
	bios_call	b_setsec
	bios_call	b_read
	or	a
	ret	z
	jp	err_read_err
;
;
; Write single record
; I: IY: ID Block
;    ?@record_area set
;    @relrec set
; O: (?@record_area) written to record
;
write_record:
	call	compute_trkrec			;convert @relrec -> @trk,@sec
	ld	bc,(?@record_area)
	bios_call	b_setdma
	ld	bc,(@track)
	bios_call	b_settrk
	ld	bc,(@record)
	bios_call	b_setsec
	bios_call	b_write
	or	a
	ret	z
	jp	err_write_err
;
;
; Convert @cluster to @lsn
; I: IY -> ID Block
;    @cluster set
; O: @lsn set
;
compute_lsn:
	ld	hl,(@cluster)
	dec	hl
	dec	hl
	ex	de,hl
	ld	b,(iy+.spc)
	ld	hl,0
compute_lsn_loop:
	add	hl,de
	djnz	compute_lsn_loop
	ld	e,(iy+.data)
	ld	d,(iy+.data+1)
	add	hl,de
	ld	(@lsn),hl
	ret
;
;
; Convert @lsn to @relrec
; I: IY -> ID Block
;    @lsn set
; O: @relrec set
;
compute_relrec:
	ld	hl,(@lsn)
	ld	a,(iy+.rps)
	call	multiply			;hl = @lsn * .rps
	ld	e,(iy+.off)
	ld	d,(iy+.off+1)
	add	hl,de
	ld	(@relrec),hl
	ret
;
;
; Convert @relrec to @track, @record
; I: IY -> ID Block
;    @relrec set
; O: @track set
;    @record set
;
compute_trkrec:
	ld	hl,(@relrec)
	ld	bc,-1
	ld	e,(iy+.rpt)
	ld	d,0				;de = recs per track
compute_track_loop:
	inc	bc
	or	a
	sbc	hl,de
	jr	nc,compute_track_loop
	add	hl,de
	ld	a,(iy+.sides)
	cp	2				;double sided ?
	jr	z,set_track			;Y: jp
	sla	c				;N: track * 2
	rl	b
set_track:
	ld	(@track),bc
	ld	(@record),hl
	ret
;
;
; Select Destination Drive
; I: @dest_code set
; O: drive selected via BIOS-Call
;
select_dest:
	ld	a,(@dest_code)
	ld	c,a
	ld	e,0
	bios_call	b_seldsk
	ld	a,h
	or	l
	ret	nz
	jp	err_dest_not_ready
;
;
; Multiply HL by A
; I: HL, A
; O: HL = HL * A
;
multiply:
	ld	e,a
	ld	d,0
	ld	c,l
	ld	b,h
	ld	hl,0
	ld	a,16
multiply_loop:
	add	hl,hl
	ex	de,hl
	add	hl,hl
	ex	de,hl
	jr	nc,multiply_noadd
	add	hl,bc
multiply_noadd:
	dec	a
	jr	nz,multiply_loop
	ret
;----------------------------------------------------------
;
; get from name
; I: @fcb set
; O: @from_fcb set
;
get_from_name:
	ld	a,(@fcb)			;from disk given ?
	or	a
	jr	nz,put_src_drv			;Y: ok
	bdos_call	curdsk			;N: use curdsk
	inc	a
put_src_drv:
	ld	(@from_fcb),a			;save from dsk
	dec	a
	cp	16
	jp	nc,err_bad_src_drv
	ld	(@src_code),a
	add	a,'A'
	ld	(@src_drive),a
	xor	a
	ld	(@joker?),a
	ld	hl,@fcb+1
	ld	a,(hl)
	cp	space				;source name given ?
	jp	z,err_no_src_name		;N: error
	ld	de,@from_name
	ld	b,11
copy_from_name:
	ld	a,(hl)
	ld	(de),a
	cp	'?'
	jr	nz,not_a_joker
	ld	a,0ffh
	ld	(@joker?),a
not_a_joker:
	inc	hl
	inc	de
	djnz	copy_from_name
	ld	b,4
clear_from_loop:
	ld	(de),0
	inc	de
	djnz	clear_from_loop
	ret
;
;
; get destination name
; I: @fcb+16 set
; O: @to_name set
;
get_to_name:
	ld	a,(@fcb+16)			;to drive given ?
	or	a
	jp	z,err_no_dest_drv		;N: error
	dec	a
	cp	16
	jp	nc,err_bad_dest_drv
	ld	(@dest_code),a
	add	a,'A'
	ld	(@dest_drive),a
	ld	hl,@fcb+17
	ld	a,(hl)
	cp	space				;destname given ?
	jr	nz,put_to_name			;Y: ok, use it
	ld	hl,@fcb+1			;N: use sourcename
put_to_name:
	ld	de,@to_name
	ld	bc,11
	ldir
	ret
;----------------------------------------------------------
;
;
break?:
	push	hl,bc
	bdos_call	drctio,255
	pop	bc,hl
	cp	3
	ret	nz
	jp	quit
;
?bios_call:
	ld	hl,<?@bios: dw 0>
	ld	l,a
	jp	(hl)
;
reset_system:
	bdos_call	13
	ret
;
; Reset data_area parms
;
reset_data:
	ld	hl,@data_area			;set buffer pointer to start
	ld	(?@data_area),hl
	xor	a
	ld	(@data_len),a
	ret
;
;
; Gibt Registerinhalt HL als Hexstring in DE .. DE+3
;
hexasc:
	ld	a,h
      4:srl	a
	call	ascwr
	ld	a,h
	and	00001111b
	call	ascwr
	ld	a,l
      4:srl	a
	call	ascwr
	ld	a,l
	and	00001111b
;
;
; write ascii char
;
ascwr:
	add	a,'0'
	cp	':'
	jr	c,nexnib
	add	a,7
nexnib:
	ld	(de),a
	inc	de
	ret
;
;
;
reset_all_parms:
	xor	a
	ld	(@eof_pos),a
	ld	(@data_full?),a
	ld	(@data_len),a
	ld	(@entry_#),a
	ld	hl,-1
	ld	(@length),hl
	ld	(@read_len),hl
	ld	(@write_len),hl
	ld	(@relrec),hl
	ld	(@record),hl
	ld	(@track),hl
	ld	hl,@data_area
	ld	(?@cluster_area),hl
	ld	(?@lsn_area),hl
	ld	(?@record_area),hl
	ld	(?@data_area),hl
	ret
;
;
; Show what we're copying
;
start_copying:
	ld	hl,@src_name
	ld	de,from_filename
	ld	a,'.'
	call	copy_names
	ld	a,(@src_drive)
	ld	(from_drive),a
	ld	hl,@dest_name
	ld	de,to_filename
	ld	a,'.'
	call	copy_names
	ld	a,(@dest_drive)
	ld	(to_drive),a
	print	txt_start_copying
	ret
;
copy_names:
	ld	bc,8
	ldir
	ld	(de),a
	inc	de
	ld	bc,3
	ldir
	ret
